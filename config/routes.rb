Rails.application.routes.draw do
  devise_for :users
  root to: 'welcome#index'
  resources :docs
  authenticated :user do
    devise_scope :user do
      root to: "docs#index", :as => "authenticated_root"
    end
  end



  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
