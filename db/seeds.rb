# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
Doc.delete_all
User.delete_all

user = User.create(email: 'test@test.com', password: 'asdfasdf', password_confirmation: 'asdfasdf')
Doc.create(title: 'Lorem Ipsum !', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 Nunc rutrum eu nisi vel bibendum. Interdum et malesuada fames ac ante ipsum primis in faucibus.
Morbi viverra nulla vitae posuere viverra. Etiam aliquet bibendum nunc. Donec a nulla erat.
 Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
Quisque iaculis nunc id libero sagittis aliquet. Aenean et lorem eget elit interdum aliquet.
Duis vitae arcu dictum, convallis erat eu, aliquam nisi. Sed consectetur pulvinar turpis non lobortis.
 Nam tortor ex, interdum sed libero ac, luctus pellentesque velit. Sed augue purus,
 pharetra semper purus vel, laoreet faucibus nulla. In vitae ultricies sem. ', user: user)

Doc.create(title: 'Lorem Ipsum 2', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 Nunc rutrum eu nisi vel bibendum. Interdum et malesuada fames ac ante ipsum primis in faucibus.
Morbi viverra nulla vitae posuere viverra. Etiam aliquet bibendum nunc. Donec a nulla erat.
 Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
Quisque iaculis nunc id libero sagittis aliquet. Aenean et lorem eget elit interdum aliquet.
Duis vitae arcu dictum, convallis erat eu, aliquam nisi. Sed consectetur pulvinar turpis non lobortis.
 Nam tortor ex, interdum sed libero ac, luctus pellentesque velit. Sed augue purus,
 pharetra semper purus vel, laoreet faucibus nulla. In vitae ultricies sem. ', user: user)

Doc.create(title: 'Lorem Ipsum 3', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 Nunc rutrum eu nisi vel bibendum. Interdum et malesuada fames ac ante ipsum primis in faucibus.
Morbi viverra nulla vitae posuere viverra. Etiam aliquet bibendum nunc. Donec a nulla erat.
 Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
Quisque iaculis nunc id libero sagittis aliquet. Aenean et lorem eget elit interdum aliquet.
Duis vitae arcu dictum, convallis erat eu, aliquam nisi. Sed consectetur pulvinar turpis non lobortis.
 Nam tortor ex, interdum sed libero ac, luctus pellentesque velit. Sed augue purus,
 pharetra semper purus vel, laoreet faucibus nulla. In vitae ultricies sem. ', user: user)